#! /usr/bin/env bash

folder="$(echo "$1" | sed -e "s/^\///" -e "s/\/\$//")"
backupdir="$(echo "$2" | sed -e "s/\/\$//")"

mkdir -p "$backupdir"

tar -czC / "$folder/" -f "$backupdir/backup_$(date +'%y%m%d%H%M%S').tar.gz"

[ $(ls "$backupdir" | wc -l) -gt 5 ] && (cd "$backupdir" && rm "$(ls | head -n 1)")
